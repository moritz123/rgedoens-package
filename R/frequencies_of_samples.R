#' This is freq
#'
#' @param dataFrameList ist Liste
#' @param norm Sollen die Listenelemente(Samples) normiert werden?
#' @param title Setzt Titel des Plots.
#' @param max.y.value Begrenzt die y-Achse.
#' @param group.pattern Definiert Gruppen für dessen Elemente jeweils das gleiche Symbol im Plot verwendet wird. Die durch die Patterns definierten Gruppen müssen disjunkt sein.
#' @param font.size Setzen der Schriftgröße im Plot.
#' @param angle.x.label Schifstellwinkel der X-Achsen-Label
#' @param x.label.vjust Verticales Offset der X-Achsen-Label(nützlich wenn beim Schiefstellen Label in den Plot ragen).
#' @examples
#' frequencies_of_samples_ggplot(l.test, norm=T, max.y.value = 0.3,
#' group.pattern = c('Mouse 1' = 'm1','Mouse 2' = 'm2'))
#' @export
#' @importFrom RColorBrewer brewer.pal
#' @import ggplot2

frequencies_of_samples_ggplot <- function(dataFrameList, norm = T, title = NULL, 
    max.y.value = NULL, group.patterns = NULL, font.size = 20, angle.x.label = 0, 
    x.label.vjust = 0) {
    l <- dataFrameList
    
    if (norm) 
        values.l <- lapply(l, function(df) {
            df[, 1] * 100/sum(df[, 1])
        }) else values.l <- lapply(l, function(df) df[, 1])
    
    # define here, if group.patterns == NULL
    group.index.vec <- 1:length(l)
    names(group.index.vec) <- group.index.vec
    if (!is.null(group.patterns)) {
        # get index of group.pattern in names(l)
        groups <- lapply(group.patterns, function(pattern) {
            grep(pattern, names(l))
        })
        names(groups) <- group.patterns
        
        for (i in seq_along(groups)) {
            group.index.vec[groups[[i]]] <- i
            names(group.index.vec)[groups[[i]]] <- names(groups)[i]
        }
        
        # get char names
        groups <- lapply(groups, function(i) {
            names(l)[i]
        })
    }
    
    # make df
    df.plot <- NULL
    for (i in seq_along(values.l)) {
        values <- values.l[[i]]
        df.plot <- rbind(df.plot, data.frame(number = 1:length(values), source = rep(names(l)[i], 
            length(values)), abun = values, group = rep(names(group.index.vec)[i], 
            length(values))))
    }
    
    cbbPalette <- RColorBrewer::brewer.pal(n = length(l) + 1, name = "Set1")[-6]
    
    p <- ggplot(df.plot, aes(x = number, y = abun, fill = source, shape = source, 
        color = source, group = source), legend = F) + geom_point() + scale_x_log10(labels = non_scientific) + 
        xlab("") + ylab("Percentage") + scale_colour_manual(name = "Samples", values = cbbPalette) + 
        scale_fill_hue(name = "Samples") + theme(text = element_text(size = font.size), 
        axis.text.x = element_text(angle = angle.x.label, vjust = x.label.vjust))
    
    if (!is.null(group.patterns)) {
        p <- p + scale_shape_manual(name = "Samples", values = as.vector(group.index.vec))
    } else {
        p <- p + scale_shape_manual(name = "Samples", values = seq_along(l))
    }
    
    if (!is.null(max.y.value)) {
        p <- p + ylim(c(0, max.y.value))
    }
    
    if (!is.null(title)) {
        p <- p + ggtitle(title)
    }
    
    return(p)
}

non_scientific <- function(l) {
    # turn in to character string in non scientific notation
    l <- format(l, scientific = FALSE)
}
