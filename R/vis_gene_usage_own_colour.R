#' vis.gene.usage erweitert um eigene Farbwahl
#'
#' Benutzt tcr::vis.gene.usage zum Ploten.
#'
#' @param l A list of data.frames
#' @param .colours Definiert die Farben fuer die Samples
#'
#' @examples
#' # Bsp: library(RColorBrewer)
#' cbbPalette <- brewer.pal(n = 8, name = 'Set1')[-6]
#' vis_gene_usage_own_colour(lapply(l, renameToTcR2), .genes = MOUSE_TRBV, .dodge=T, .norm=T, .colours = c(as.vector(sapply(cbbPalette, function(x) rep(x,2)))))
#' @export
#' @importFrom tcR geneUsage
#' @importFrom data.table melt
#' @import ggplot2

vis_gene_usage_own_colour <- function(l, .genes = NULL, .main = "Gene usage", .ncol = 3, 
    .coord.flip = F, .dodge = T, .labs = c("Gene", "Frequency"), .colours = NULL, 
    ...) {
    .data <- lapply(l, rgedoens::pp_rename_to_tcr2)
    if (is.null(.genes)) 
        stop(".genes is NULL. Please set the .genes Parameter!")
    if (is.null(.colours)) 
        warning("Colors is NULL. Using default colors.")
    
    if (!is.na(.genes[1])) {
        res <- geneUsage(.data, .genes, ...)
    } else {
        res <- .data
    }
    if (class(res[[2]]) != "factor") {
        res <- data.table::melt(res)
        res <- res[1:nrow(res), ]
        colnames(res) <- c("Gene", "Sample", "Freq")
    }
    if (length(unique(res$Sample)) > 1) {
        if (.dodge) {
            p <- ggplot() + geom_bar(aes(x = Gene, y = Freq, fill = Sample), data = res, 
                stat = "identity", position = position_dodge(), colour = "black") + 
                theme_linedraw() + theme(axis.text.x = element_text(angle = 90)) + 
                scale_y_continuous(expand = c(0.02, 0))
            
            if (is.null(.colours)) {
                p <- p + .colourblind.discrete(length(unique(res$Sample)))
            } else {
                if (length(.colours) < length(unique(res$Sample))) 
                  stop("Insufficient values in .colours. ", length(unique(res$Sample)), 
                    " needed but only ", length(.colours), " provided")
                p <- p + scale_fill_manual(values = .colours)
            }
            
            return(p)
        } else {
            res <- split(res, res$Sample)
            ps <- lapply(1:length(res), function(i) {
                vis.gene.usage(res[[i]], NA, names(res)[i], 0, .coord.flip, .labs = .labs, 
                  ...)
            })
            do.call(grid.arrange, c(ps, ncol = .ncol, top = .main))
        }
    } else {
        p <- ggplot() + geom_bar(aes(x = Gene, y = Freq, fill = Freq), data = res, 
            stat = "identity", colour = "black")
        if (.coord.flip) {
            p <- p + coord_flip()
        }
        p + theme_linedraw() + theme(axis.text.x = element_text(angle = 90)) + ggtitle(.main) + 
            .colourblind.gradient() + scale_y_continuous(expand = c(0.02, 0)) + xlab(.labs[1]) + 
            ylab(.labs[2])
    }
}

# white/orange/yellow - green - blue colourblind - friendly for fill and colour
.colourblind.discrete <- function(.n, .colour = F) {
    # cs <- c('#FFFFD9', '#41B6C4', '#225EA8') cs <- c('#FFFFBB', '#41B6C4',
    # '#225EA8') cs <- c('#FFBB00', '#41B6C4', '#225EA8') <- old version cs <-
    # c('#FF4B20', '#FFB433', '#C6FDEC', '#7AC5FF', '#0348A6')
    cs <- c("#FF4B20", "#FFB433", "#C6FDEC", "#7AC5FF", "#0348A6")
    if (.colour) {
        scale_colour_manual(values = colorRampPalette(cs)(.n))
    } else {
        scale_fill_manual(values = colorRampPalette(cs)(.n))
    }
}
