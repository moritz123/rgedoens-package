---
title: "Rgedoens Vignette"
author: "Hiwi"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Package load and test

Load package

```{r, fig.show='hold'}
library(rgedoens)
data(l.test)
```

Preprocess data samples. Function autodetect AA and nuc column. 
Cut values after first ',' in V/J.segments column.
Output are the detected column names.

```{r, fig.show='hold'}
l.test <- pp_l(l.test, cut.vj = T)
```

Draw test plot

```{r, fig.height=5, fig.width=7}
cdr3_aa_length_grouped_bar_mean(l.test)
```

##CDR3 plots:

```{r, fig.height=5, fig.width=7}
cdr3_aa_length_grouped_bar_mean(l.test, group.regex = c("Mouse 1" = "m1","Mouse 2" = "m2","Mouse 3" = "m3"), norm = T)
```

```{r, fig.height=5, fig.width=7}
cbbPalette <- RColorBrewer::brewer.pal(n = 4, name = 'Set1')
cdr3_length_grouped_barplot_mean_subsample(l.test[1:6], subsampleCount = 100, runs = 10, 
                                           cbPalette = cbbPalette, 
                                           group.regex = c("Mouse 1" = "m1","Mouse 2" = "m2"))
```

```{r, fig.height=5, fig.width=7}
heatmap_shared_clonotypes_orange_mean(l.test[c(1,2,4,5)], subsampleCount = 500, fontSizeMap = 1)
```

```{r, fig.height=5, fig.width=7}
heatmap_shared_clonotypes_with_percent(l.test[c(3,6,9)], fontSizeMap = 1)
```

The venn function supports a maximum of 5 samples!

```{r, fig.height=5, fig.width=7}
venn_diagram(l.test[1:5])
```

Error if you call venn with a list with more than 5 data.frames.

```{r, purl = FALSE, error=TRUE}
venn_diagram(l.test)
```



##Moeller Plots
The following are only Moeller-Plots:

Coverage of Clonotypes in different samples:

```{r, fig.height=5, fig.width=7}
coverage_of_clonotypes(l.test[c(1,4,7)], intervals = c(0, 0.1, 0.2), 
                       intervals.label = c("S", "M", "L"), 
                       intervals.label.legend = c("auto", "auto", "auto"))
```


```{r, fig.height=5, fig.width=7}
coverage_of_clonotypes(l.test[c(1,4,7)], intervals = c(0, 0.1, 0.2), 
                       intervals.label = c("S", "M", "L"), 
                       intervals.label.legend = c("auto", "auto", "auto"), wo.norm = T)
```

Relative observability of Clonotypes in different samples:

```{r, fig.height=5, fig.width=7}
relative_observability_of_clonotypes(l.test[c(1,4,7)], intervals = c(0, 0.1, 0.2), 
                                     intervals.label = c("S", "M", "L"), 
                                     intervals.label.legend = c("auto", "auto", "auto"))
```


```{r, fig.height=5, fig.width=7}
relative_observability_of_clonotypes(l.test[c(1,4,7)], intervals = c(0, 0.1, 0.2), 
                                     intervals.label = c("S", "M", "L"), 
                                     intervals.label.legend = c("auto", "auto", "auto"), wo.norm = T)
```

```{r, fig.height=5, fig.width=7}
moeller_frac_abun(l.test, mode = "TCZs in blood", together = FALSE, cutRunaways = FALSE,
 cutRunawaysMaxFactor = 3, maxScale = 0, maxMice = 0, countBound = 0)
```

```{r, fig.height=5, fig.width=7}
moeller_frac_abun(l.test, mode = "TCZs in blood", together = FALSE, cutRunaways = FALSE,
 cutRunawaysMaxFactor = 3, maxScale = 2, maxMice = 2, countBound = 100)
```

```{r, fig.height=5, fig.width=7}
moeller_scatter_tcz(l.test, threshold.tcz.list = c(0.01), mouse.names = c("mouse 1", "mouse 7", "mouse 13"))
```



##VJ-Usage:

Plot the V or J-usage for different samples as a histogram.
Each bar in the plot is a mean over all samples in one group.

```{r, fig.height=5, fig.width=7}
vj_usage_grouped_mean_histogram(l.test, v.usage=T, 
                                group.regex = c("Group m1" = "m1", "Group m2" = "m2", "Group m3" = "m3"), 
                                plot.sd.error.bars = T)
```

```{r, fig.height=5, fig.width=7}
vj_usage_grouped_mean_histogram(l.test, v.usage=F, 
                                group.regex = c("Group m1" = "m1", "Group m2" = "m2", "Group m3" = "m3"), 
                                plot.sd.error.bars = T)
```

vis.gene.usage extended for own colors. This functions use tcr::vis.gene.usage

```{r, fig.height=5, fig.width=7}
cbbPalette <- RColorBrewer::brewer.pal(n = 6, name = 'Set1')
vis_gene_usage_own_colour(l.test[-c(3,6,9)], .genes = MOUSE_TRBV, .colours = cbbPalette)
```




```{r, fig.height=5, fig.width=7}
frequencies_of_samples_ggplot(l.test[c(1,4,7)], norm=T, group.pattern = c("Mouse 1" = "m1","Mouse 2" = "m2"))
```

```{r, fig.height=5, fig.width=7}
plot_total_insertion(l.test[1:3], range = 0:20)
```

```{r, fig.height=5, fig.width=7}
chisq_plot(l.test[1:2], legend.y.names = c("T1","T2"), title = "Distribution between 2 TCZ of mouse m1 naive using Chi-squared Test",
              range = 1:20, max.value = 0, ignore.own.range = F, color.numbers = "goldenrod3")
```

Compares the abundance of a range of clontypes in one Sample with other Samples.

```{r, fig.height=5, fig.width=7}
sample_abun_comparison(l.test[1:2], max.y.value = NULL, sample.range = 1:20, cols = NULL, 
                       title = NULL, pch = 19, cex = 2, pch.null = 4, cex.null = 2, 
                       color.off = 0.1, marked.AA.labels = NULL, marked.AA.labels.col = "red4", 
                       writePos = T, not.all= T, writePos.angle = 0, writePos.offset = 0.03)
```

```{r, fig.height=5, fig.width=7}
private_shared_public_sum_histogram(l.test[-c(3,6,9)], range = 1:20, 
                                    partner.vec = c(2, 1, 4, 3, 6, 5), ret = F, norm = F)
```

##Help functions

Get the AA intersection count between samples like in the venn diagram.
```{r, fig.show='hold'}
get_venn_intersection(l.test[1:3])
```

Return the venn intersection AA's between m1TCZ1, m1TCZ2 and m1Blood.
```{r, fig.show='hold'}
get_venn_intersection(l.test[1:3], c("m1TCZ1", "m1TCZ2", "m1Blood"))
```

Helpful marco if you want the intersection between multiple samples.
```{r, fig.show='hold'}
df_view_sample_1 <- get_intersection_df(l.test)
```

If you want the "view" from another sample, simply write the sample number to the front.
Internally R now calls the function with a 10 element list, but the double element 3 do not care in the intersection function here.
```{r, fig.show='hold'}
df_view_sample_3 <- get_intersection_df(l.test[c(3,1:9)])
```



##Sample Stuff

Vignettes are long form documentation commonly included in packages. Because they are part of the distribution of the package, they need to be as compact as possible. The `html_vignette` output type provides a custom style sheet (and tweaks some options) to ensure that the resulting html is as small as possible. The `html_vignette` format:

- Never uses retina figures
- Has a smaller default figure size
- Uses a custom CSS stylesheet instead of the default Twitter Bootstrap style

## Vignette Info

Note the various macros within the `vignette` section of the metadata block above. These are required in order to instruct R how to build the vignette. Note that you should change the `title` field and the `\VignetteIndexEntry` to match the title of your vignette.

## Styles

The `html_vignette` template includes a basic CSS theme. To override this theme you can specify your own CSS in the document metadata as follows:

    output: 
      rmarkdown::html_vignette:
        css: mystyles.css

## Figures

You can enable figure captions by `fig_caption: yes` in YAML:

    output:
      rmarkdown::html_vignette:
        fig_caption: yes

Then you can use the chunk option `fig.cap = "Your figure caption."` in **knitr**.

## More Examples

You can write math expressions, e.g. $Y = X\beta + \epsilon$, footnotes^[A footnote here.], and tables, e.g. using `knitr::kable()`.

```{r, echo=FALSE, results='asis'}
knitr::kable(head(mtcars, 10))
```

Also a quote using `>`:

> "He who gives up [code] safety for [code] speed deserves neither."
([via](https://twitter.com/hadleywickham/status/504368538874703872))
